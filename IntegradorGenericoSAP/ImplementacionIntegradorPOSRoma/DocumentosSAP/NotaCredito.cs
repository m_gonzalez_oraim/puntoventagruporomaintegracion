﻿using IntegradorGenericoSAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ImplementacionIntegradorPOSRoma.DocumentosSAP
{
    public class NotaCredito : IObjetoSincronizacionSAP
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        private WrapperConexionSAP wrapperConexionSAP;
        public WrapperConexionSAP WrapperConexionSAP
        {
            set { wrapperConexionSAP = value; }
        }

        private string mensajeError;
        public string MensajeError
        {
            get { return mensajeError; }
        }

        private System.Data.DataSet informacionNotaCredito;
        public System.Data.DataSet InformacionObjeto
        {
            get
            {
                return informacionNotaCredito;
            }
            set
            {
                informacionNotaCredito = value;
            }
        }

        public bool Sincronizar()
        {
            try
            {
                string codigoClienteSAP;
                if (!validarInformacionIntegracion(out codigoClienteSAP))
                    return false;


                if (documentoYaHaSidoIntegrado((int)InformacionObjeto.Tables[0].Rows[0]["IDStore"], (int)InformacionObjeto.Tables[0].Rows[0]["IDInternoPOS"]))
                    return true;

                SAPbobsCOM.Documents notaCredito = NotaCreditoSAP(codigoClienteSAP);
                int codigoCreacionNotaCredito = notaCredito.Add();
                if (codigoCreacionNotaCredito != 0)
                {
                    mensajeError = wrapperConexionSAP.Conexion.GetLastErrorCode() + "-" + wrapperConexionSAP.Conexion.GetLastErrorDescription();
                    return false;
                }

                return true;
            }
            catch(Exception ex)
            {
                logger.ErrorException("Sincronizar", ex);
                mensajeError = "Excepcion no controlada al sincronizar el objeto."+ex.Message;
                return false;
            }
        }

        private SAPbobsCOM.Documents NotaCreditoSAP(string codigoClienteSAP)
        {
            System.Data.DataTable tablaTickets = InformacionObjeto.Tables[1];

            StringBuilder definicionArchivoIntegracion= new StringBuilder();
            
            XmlWriter generadorArchivoIntegracionSAP = XmlWriter.Create(definicionArchivoIntegracion);
            generadorArchivoIntegracionSAP.WriteStartDocument();
            generadorArchivoIntegracionSAP.WriteStartElement("BOM");
            generadorArchivoIntegracionSAP.WriteStartElement("BO");

            generadorArchivoIntegracionSAP.WriteStartElement("AdmInfo");
            generadorArchivoIntegracionSAP.WriteStartElement("Object");
            generadorArchivoIntegracionSAP.WriteValue("17");
            generadorArchivoIntegracionSAP.WriteEndElement();
            generadorArchivoIntegracionSAP.WriteStartElement("Version");
            generadorArchivoIntegracionSAP.WriteValue("2");
            generadorArchivoIntegracionSAP.WriteEndElement();

            generadorArchivoIntegracionSAP.WriteStartElement("Documents");
            generadorArchivoIntegracionSAP.WriteStartElement("row");
            
            generadorArchivoIntegracionSAP.WriteStartElement("DocType");
            generadorArchivoIntegracionSAP.WriteValue("dDocument_Items");
            generadorArchivoIntegracionSAP.WriteEndElement();
            generadorArchivoIntegracionSAP.WriteStartElement("CardCode");
            generadorArchivoIntegracionSAP.WriteValue(codigoClienteSAP);
            generadorArchivoIntegracionSAP.WriteEndElement();
            generadorArchivoIntegracionSAP.WriteStartElement("CardName");
            generadorArchivoIntegracionSAP.WriteValue(InformacionObjeto.Tables[0].Rows[0]["NombreSocioNegocios"]);
            generadorArchivoIntegracionSAP.WriteEndElement();
            generadorArchivoIntegracionSAP.WriteStartElement("DocDate");
            generadorArchivoIntegracionSAP.WriteValue(((DateTime)InformacionObjeto.Tables[0].Rows[0]["FechaTransaccion"]).ToString("yyyy-MM-dd"));
            generadorArchivoIntegracionSAP.WriteEndElement();
            generadorArchivoIntegracionSAP.WriteStartElement("TaxDate");
            generadorArchivoIntegracionSAP.WriteValue(((DateTime)InformacionObjeto.Tables[0].Rows[0]["FechaTransaccion"]).ToString("yyyy-MM-dd"));
            generadorArchivoIntegracionSAP.WriteEndElement();
            generadorArchivoIntegracionSAP.WriteStartElement("U_BXPTiendaPOS");
            generadorArchivoIntegracionSAP.WriteValue(InformacionObjeto.Tables[0].Rows[0]["IDStore"]);
            generadorArchivoIntegracionSAP.WriteEndElement();
            generadorArchivoIntegracionSAP.WriteStartElement("U_BXPIdPOS");
            generadorArchivoIntegracionSAP.WriteValue(InformacionObjeto.Tables[0].Rows[0]["IDInternoPOS"]);
            generadorArchivoIntegracionSAP.WriteEndElement();
            generadorArchivoIntegracionSAP.WriteStartElement("U_BXPFolioPOS");
            generadorArchivoIntegracionSAP.WriteValue(InformacionObjeto.Tables[0].Rows[0]["FolioPOS"]);
            generadorArchivoIntegracionSAP.WriteEndElement();
            generadorArchivoIntegracionSAP.WriteStartElement("TotalDiscount");
            generadorArchivoIntegracionSAP.WriteValue(InformacionObjeto.Tables[0].Rows[0]["MontoDescuentoGlobal"]);
            generadorArchivoIntegracionSAP.WriteEndElement();
            generadorArchivoIntegracionSAP.WriteStartElement("U_BXPComentarios");
            generadorArchivoIntegracionSAP.WriteValue(InformacionObjeto.Tables[0].Rows[0]["Comentarios"]);
            generadorArchivoIntegracionSAP.WriteEndElement();
            generadorArchivoIntegracionSAP.WriteStartElement("DocCurrency");
            generadorArchivoIntegracionSAP.WriteValue(InformacionObjeto.Tables[0].Rows[0]["Moneda"]);
            generadorArchivoIntegracionSAP.WriteEndElement();
            generadorArchivoIntegracionSAP.WriteStartElement("DocRate");
            generadorArchivoIntegracionSAP.WriteValue(InformacionObjeto.Tables[0].Rows[0]["TipoCambio"]);
            generadorArchivoIntegracionSAP.WriteEndElement();

            generadorArchivoIntegracionSAP.WriteEndElement();
            generadorArchivoIntegracionSAP.WriteEndElement();

            generadorArchivoIntegracionSAP.WriteStartElement("Document_Lines");
            for(int i=0;i<tablaTickets.Rows.Count;i++)
            {
                generadorArchivoIntegracionSAP.WriteStartElement("row");

                generadorArchivoIntegracionSAP.WriteStartElement("ItemCode");
                generadorArchivoIntegracionSAP.WriteValue(tablaTickets.Rows[i]["CodigoProducto"]);
                generadorArchivoIntegracionSAP.WriteEndElement();
                generadorArchivoIntegracionSAP.WriteStartElement("ItemName");
                generadorArchivoIntegracionSAP.WriteValue(tablaTickets.Rows[i]["NombreProducto"]);
                generadorArchivoIntegracionSAP.WriteEndElement();
                generadorArchivoIntegracionSAP.WriteStartElement("Quantity");
                generadorArchivoIntegracionSAP.WriteValue(tablaTickets.Rows[i]["Cantidad"]);
                generadorArchivoIntegracionSAP.WriteEndElement();
                generadorArchivoIntegracionSAP.WriteStartElement("UnitPrice");
                generadorArchivoIntegracionSAP.WriteValue(tablaTickets.Rows[i]["PrecioUnitario"]);
                generadorArchivoIntegracionSAP.WriteEndElement();
                generadorArchivoIntegracionSAP.WriteStartElement("DiscountPercent");
                generadorArchivoIntegracionSAP.WriteValue(tablaTickets.Rows[i]["PorcentajeDescuento"]);
                generadorArchivoIntegracionSAP.WriteEndElement();
                generadorArchivoIntegracionSAP.WriteStartElement("TaxCode");
                generadorArchivoIntegracionSAP.WriteValue(tablaTickets.Rows[i]["CodigoImpuesto"]);
                generadorArchivoIntegracionSAP.WriteEndElement();
                generadorArchivoIntegracionSAP.WriteStartElement("WarehouseCode");
                generadorArchivoIntegracionSAP.WriteValue(tablaTickets.Rows[i]["AlmacenOrigen"]);
                generadorArchivoIntegracionSAP.WriteEndElement();
                generadorArchivoIntegracionSAP.WriteStartElement("U_BXPFactor");
                generadorArchivoIntegracionSAP.WriteValue(tablaTickets.Rows[i]["Factor"]);
                generadorArchivoIntegracionSAP.WriteEndElement();
                generadorArchivoIntegracionSAP.WriteStartElement("U_BXPCantidad");
                generadorArchivoIntegracionSAP.WriteValue(tablaTickets.Rows[i]["CantidadUnidadMedida"]);
                generadorArchivoIntegracionSAP.WriteEndElement();
                generadorArchivoIntegracionSAP.WriteStartElement("U_BXPPrecio");
                generadorArchivoIntegracionSAP.WriteValue(tablaTickets.Rows[i]["PrecioUnidadMedida"]);
                generadorArchivoIntegracionSAP.WriteEndElement();
                generadorArchivoIntegracionSAP.WriteStartElement("U_BXPLineaPOS");
                generadorArchivoIntegracionSAP.WriteValue(tablaTickets.Rows[i]["Linea"]);
                generadorArchivoIntegracionSAP.WriteEndElement();
                generadorArchivoIntegracionSAP.WriteStartElement("U_BXPIdPOS");
                generadorArchivoIntegracionSAP.WriteValue(tablaTickets.Rows[i]["IDInternoPOS"]);
                generadorArchivoIntegracionSAP.WriteEndElement();
                
                generadorArchivoIntegracionSAP.WriteEndElement();
            }
            generadorArchivoIntegracionSAP.WriteEndElement();

            generadorArchivoIntegracionSAP.WriteEndElement();
            generadorArchivoIntegracionSAP.WriteEndElement();

            return wrapperConexionSAP.Conexion.GetBusinessObjectFromXML(generadorArchivoIntegracionSAP.ToString(), 0);
        }

        private bool facturaSeEncuentraIntegrada(int idFactura,int idStore)
        {
            SAPbobsCOM.Recordset recordSet = null;
            string queryFacturaIntegrada = string.Format("SELECT DocEntry FROM OINV WHEREU_BXPIdPOS={1} AND U_BXPTiendaPOS={0} ", idFactura,idStore);
            try
            {
                recordSet = (SAPbobsCOM.Recordset) wrapperConexionSAP.Conexion.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                recordSet.DoQuery(queryFacturaIntegrada);
                if (recordSet.RecordCount == 0)
                    return false;
                else
                    return true;
            }
            finally
            {
                if(recordSet != null)
                {
                    Marshal.ReleaseComObject(recordSet);
                    recordSet = null;
                }
            }
        }

        private string codigoClienteSAP(int socioNegociosPOS)
        {
            SAPbobsCOM.Recordset recordSet = null;
            string queryClienteIntegradoEnSAP = string.Format("SELECT CardCode FROM OCRD WHERE U_BXP_CodPOS={0}", socioNegociosPOS);
            try
            {
                recordSet = (SAPbobsCOM.Recordset)wrapperConexionSAP.Conexion.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                recordSet.DoQuery(queryClienteIntegradoEnSAP);
                if (recordSet.RecordCount == 0)
                    return null;
                return (string)recordSet.Fields.Item(0).Value;
            }
            finally
            {
                if (recordSet != null)
                {
                    Marshal.ReleaseComObject(recordSet);
                    recordSet = null;
                }
            }
        }

        private bool documentoYaHaSidoIntegrado(int idTienda, int idPOS)
        {
            SAPbobsCOM.Recordset recordSet = null;
            try
            {
                recordSet = (SAPbobsCOM.Recordset) wrapperConexionSAP.Conexion.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                string queryDocumento = 
                    string.Format("SELECT DocEntry FROM ORIN WHERE U_BXPTiendaPOS={0} AND U_BXPIdPOS={1}", idTienda, idPOS);
                recordSet.DoQuery(queryDocumento);
                if (recordSet.RecordCount == 0)
                    return false;
                else
                    return true;
            }
            catch(Exception ex)
            {
                logger.ErrorException("documentoYaHaSidoIntegrado", ex);
                return false;
            }
            finally
            {
                if(recordSet != null)
                {
                    Marshal.ReleaseComObject(recordSet);
                    recordSet = null;
                }
            }
        }

        private bool validarInformacionIntegracion(out string codigoClienteSAP)
        {
            codigoClienteSAP = null;

            int idFactura;
            int idTienda;
            int folioFactura;
            for (int i = 0; i < InformacionObjeto.Tables[1].Rows.Count;i++ )
            {
                idFactura = (int)InformacionObjeto.Tables[i].Rows[i]["IDFactura"];
                idTienda = (int)InformacionObjeto.Tables[1].Rows[i]["IDStore"];
                folioFactura = (int)InformacionObjeto.Tables[i].Rows[i]["Folio"];

                if(!facturaSeEncuentraIntegrada(idFactura,idTienda))
                {
                    mensajeError = "Factura con folio" + folioFactura + " no se ha integrado a SAP. No se puede integrar la nota de credito";
                    return false;
                }
            }

            codigoClienteSAP = (string)InformacionObjeto.Tables[0].Rows[0]["SocioNegociosSAP"];
            int codigoClientePOS = (int)InformacionObjeto.Tables[0].Rows[0]["SocioNegociosPOS"];

            if (codigoClienteSAP == string.Empty)
            {
                codigoClienteSAP = this.codigoClienteSAP(codigoClientePOS);
                if (codigoClienteSAP == null)
                {
                    mensajeError = "Cliente con codigo POS " + codigoClientePOS + " no se ha integrado aun en SAP. No se puede integrar la factura";
                    return false;
                }
            }

            return true;
        }

        public bool EjecutarOperacionesPostMarcado()
        {

            return true;
        }

        string tipoObjeto;
        public string TipoObjeto
        {
            get
            {
                return tipoObjeto;
            }
            set
            {
                tipoObjeto = value;
            }
        }
    }
}

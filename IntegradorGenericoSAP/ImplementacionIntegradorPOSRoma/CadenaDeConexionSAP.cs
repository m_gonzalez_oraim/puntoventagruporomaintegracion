﻿using IntegradorGenericoSAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImplementacionIntegradorPOSRoma
{
    public class CadenaDeConexionSAP : IConfiguradorConexionSAP
    {
        public string cadenaConexionSAP;
        public string CadenaConexionSAP
        {
            set { cadenaConexionSAP = value; }
        }
        public bool CargarInformacionConexionSAP(ref SAPbobsCOM.Company conexion)
        {
            string cadenaConexion = cadenaConexionDesencriptada();

            string servidor;
            string baseDatosCompañia;
            string usuarioBaseDatos;
            string passBaseDatos;
            string usuarioSAP;
            string passSAP;
            string servidorLicencias;
            int puertoServidorLicencias;
            SAPbobsCOM.BoDataServerTypes servidorBaseDatos;

            obtenerDatosConexionDeCadenaConexion(cadenaConexion,
                out servidor, out baseDatosCompañia,
                out usuarioSAP, out passSAP,
                out usuarioBaseDatos, out passBaseDatos,
                out servidorBaseDatos, out servidorLicencias, out puertoServidorLicencias);

            if (servidor == string.Empty ||
                baseDatosCompañia == string.Empty ||
                usuarioSAP == string.Empty ||
                passSAP == string.Empty ||
                usuarioBaseDatos == string.Empty ||
                passBaseDatos == string.Empty ||
                servidorLicencias == string.Empty ||
                puertoServidorLicencias == -1)
                return false;

            conexion.Server = servidor;
            conexion.CompanyDB = baseDatosCompañia;
            conexion.UserName = usuarioSAP;
            conexion.Password = passSAP;
            conexion.DbUserName = usuarioBaseDatos;
            conexion.DbPassword = passBaseDatos;
            conexion.DbServerType = servidorBaseDatos;
            conexion.LicenseServer = servidorLicencias + ":" + puertoServidorLicencias;

            return true;
        }

        private SAPbobsCOM.BoDataServerTypes tipoServidorBaseDeDatosDeCadena(string cadena)
        {
            if (cadena == "SQL2005")
                return SAPbobsCOM.BoDataServerTypes.dst_MSSQL2005;
            else if (cadena == "SQL2008")
                return SAPbobsCOM.BoDataServerTypes.dst_MSSQL2008;
            else if (cadena == "SQL2012")
                return SAPbobsCOM.BoDataServerTypes.dst_MSSQL2012;
            else
                return SAPbobsCOM.BoDataServerTypes.dst_MSSQL2008;
        }

        private void obtenerDatosConexionDeCadenaConexion(string cadenaDeConexion, 
            out string servidor,out string baseDatosCompañia,
            out string usuarioSAP,out string passSAP,
            out string usuarioBaseDatos,out string passBaseDatos, 
            out SAPbobsCOM.BoDataServerTypes servidorBaseDatos,
            out string servidorLicencias,out int puertoServidorLicencias)
        {
            servidor = string.Empty;
            baseDatosCompañia= string.Empty;
            usuarioSAP = string.Empty;
            passSAP = string.Empty;
            usuarioBaseDatos = string.Empty;
            passBaseDatos = string.Empty;
            servidorLicencias = string.Empty;
            puertoServidorLicencias = -1;
            servidorBaseDatos = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2008;

            string[] elementosConexion = cadenaDeConexion.Split(';');
            string[] definicionElementoConexion;
            for(int i=0;i<elementosConexion.Length;i++)
            {
                definicionElementoConexion = elementosConexion[i].Split('=');
                if (definicionElementoConexion[0].ToLower().Trim() == "server")
                    servidor = definicionElementoConexion[1];
                else if (definicionElementoConexion[0].ToLower().Trim() == "companydb")
                    baseDatosCompañia = definicionElementoConexion[1];
                else if (definicionElementoConexion[0].ToLower().Trim() == "username")
                    usuarioSAP = definicionElementoConexion[1];
                else if (definicionElementoConexion[0].ToLower().Trim() == "password")
                    passSAP = definicionElementoConexion[1];
                else if (definicionElementoConexion[0].ToLower().Trim() == "dbusername")
                    usuarioBaseDatos = definicionElementoConexion[1];
                else if (definicionElementoConexion[0].ToLower().Trim() == "dbpassword")
                    passBaseDatos = definicionElementoConexion[1];
                else if (definicionElementoConexion[0].ToLower().Trim() == "licenseserver")
                    servidorLicencias = definicionElementoConexion[1];
                else if(definicionElementoConexion[0].ToLower().Trim()=="dbservertype")
                    servidorBaseDatos = tipoServidorBaseDeDatosDeCadena(definicionElementoConexion[1]);
                else if (definicionElementoConexion[0].ToLower().Trim() == "portlicenseserver")
                {
                    if (!int.TryParse(definicionElementoConexion[1], out puertoServidorLicencias))
                        puertoServidorLicencias = -1;
                }
            }
        }

        private string cadenaConexionDesencriptada()
        {
            return cadenaConexionSAP;
        }
    }
}

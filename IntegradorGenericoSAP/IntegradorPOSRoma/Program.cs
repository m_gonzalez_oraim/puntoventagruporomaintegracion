﻿using Spring.Context;
using Spring.Context.Support;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntegradorPOSRoma
{
    class Program
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        static void Main(string[] args)
        {
            if(args.Length!=1)
            {
                logger.Debug("No se puede iniciar el proceso de sincronizacion ya que los parametros de conexion no se encuentran definidos");
                return;
            }

            IApplicationContext context = ContextRegistry.GetContext();
            ImplementacionIntegradorPOSRoma.CadenaDeConexionSAP configuradorConexion =  (ImplementacionIntegradorPOSRoma.CadenaDeConexionSAP)context.GetObject("ImplementacionConfiguradorConexionSAP");
            configuradorConexion.CadenaConexionSAP = args[0];

            IntegradorGenericoSAP.IntegradorSAP integrador = (IntegradorGenericoSAP.IntegradorSAP)context.GetObject("IntegradorSAP");
            integrador.EjecutarIntegracion();
        }
    }
}

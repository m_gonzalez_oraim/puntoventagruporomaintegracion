﻿using Spring.Data.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntegradorGenericoSAP
{
    public class IntegradorSAP
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        public List<string> filtroTiposObjetoSincronizar = null;
        public List<string> FiltroTiposObjetosSincronizar
        {
            set { filtroTiposObjetoSincronizar = value; }
        }

        private ControladorSabana controladorSabana;
        public ControladorSabana ControladorSabana
        {
            set { controladorSabana = value; }
        }

        private WrapperConexionSAP wraperConexionSAP;
        public WrapperConexionSAP WraperConexionSAP
        {
            set { wraperConexionSAP = value; }
        }

        private SAPbobsCOM.Company conexionSAP;
        public void EjecutarIntegracion()
        {
            logger.Debug("Iniciando integracion a SAP Business One");

            if (!wraperConexionSAP.ConectarseASAP())
            {
                logger.Debug("Error de conexion a SAP Business One");
                return;
            }

            logger.Debug("Obteniendo los objetos a integrar");
            while(true)
            {
                bool errorAlObtenerObjetoSincronizar;
                IObjetoSincronizacionSAP objetoSincronizacion =
                    controladorSabana.SiguienteObjetoSincronizar(filtroTiposObjetoSincronizar, out errorAlObtenerObjetoSincronizar);
                if(objetoSincronizacion==null)
                {
                    if (errorAlObtenerObjetoSincronizar)
                        logger.Debug("Se suspende la sincronizacion ya que no se pudieron obtener la informacion de la tabla de paso");
                    else
                        logger.Debug("Fin integracion a SAP Business One.");
                    break;
                }

                logger.Debug("Sincronizacion " + objetoSincronizacion.ToString());
                if (!objetoSincronizacion.Sincronizar())
                    controladorSabana.MarcarInformacionComoNoSincronizada(objetoSincronizacion);
                else
                    controladorSabana.MarcarInformacionComoSincronizada(objetoSincronizacion);
            }
        }


    }
}

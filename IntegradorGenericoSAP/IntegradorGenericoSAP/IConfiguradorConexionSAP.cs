﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntegradorGenericoSAP
{
    public interface IConfiguradorConexionSAP
    {
        public bool CargarInformacionConexionSAP(ref SAPbobsCOM.Company conexion);
    }
}

﻿using Spring.Context;
using Spring.Data.Common;
using Spring.Data.Generic;
using Spring.Transaction;
using Spring.Transaction.Support;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntegradorGenericoSAP
{
    public class ControladorSabana : IApplicationContextAware 
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        private IApplicationContext context;
        public IApplicationContext ApplicationContext
        {
            set { context = value; }
        }

        private TransactionTemplate transactionTemplate;
        public TransactionTemplate TransactionTemplate
        {
            set { transactionTemplate = value; }
        }

        private AdoTemplate template;
        public AdoTemplate Template
        {
            set { template = value; }
        }


        private int siguienteRegistroSincronizar=0;

        public IObjetoSincronizacionSAP SiguienteObjetoSincronizar(List<string> tiposObjetosSincronizar, out bool errorAlObtenerObjetoASincronizar)
        {
            errorAlObtenerObjetoASincronizar = false;

            IDbParameters parametrosObjetoSincronizar = template.CreateDbParameters();
            parametrosObjetoSincronizar.Add("SiguienteRegistro", System.Data.DbType.Int32);
            parametrosObjetoSincronizar.SetValue("SiguienteRegistro", siguienteRegistroSincronizar);
            if (tiposObjetosSincronizar!=null)
            {
                string defTiposObjetosSincronizar = tiposObjetosSincronizar.Aggregate((i, j) => i + "," + j);
                parametrosObjetoSincronizar.Add("ObjetosSincronizar",System.Data.DbType.String);
                parametrosObjetoSincronizar.SetValue("ObjetosSincronizar", defTiposObjetosSincronizar);
            }
            parametrosObjetoSincronizar.Add("RegistrosPendientes", System.Data.DbType.Boolean);
            parametrosObjetoSincronizar.Add("EjecucionCorrecta", System.Data.DbType.Boolean);
            parametrosObjetoSincronizar.Add("TipoTransaccion", System.Data.DbType.String);
            

            System.Data.DataSet informacionObjetoSincronizar = template.ClassicAdoTemplate.DataSetCreateWithParams(System.Data.CommandType.StoredProcedure, "SiguienteObjetoSincronizarSabana_SP", parametrosObjetoSincronizar);

            if (!(bool)parametrosObjetoSincronizar.GetValue("EjecucionCorrecta"))
            {
                errorAlObtenerObjetoASincronizar = true;
                return null;
            }

            if(!(bool)parametrosObjetoSincronizar.GetValue("RegistrosPendientes"))
            {
                return null;
            }

            string tipoTransaccion= (string)parametrosObjetoSincronizar.GetValue("TipoTransaccion");
            IObjetoSincronizacionSAP objetoSincronizar = (IObjetoSincronizacionSAP) context.GetObject("Sincronizacion"+tipoTransaccion);
            objetoSincronizar.InformacionObjeto =informacionObjetoSincronizar;

            siguienteRegistroSincronizar = (int)objetoSincronizar.InformacionObjeto.Tables[0].Rows[objetoSincronizar.InformacionObjeto.Tables[0].Rows.Count - 1]["Intermedia_ID"];
            return objetoSincronizar;
        }

        public bool MarcarInformacionComoNoSincronizada(IObjetoSincronizacionSAP objetoSincronizacion)
        {
            System.Data.DataTable informacionSabana = objetoSincronizacion.InformacionObjeto.Tables[0];

            IDbParameters paramsMarcarInformacion = template.CreateDbParameters();
            paramsMarcarInformacion.Add("Intermedia_ID", System.Data.DbType.Int32);
            paramsMarcarInformacion.Add("DescripcionErrorSAP", System.Data.DbType.String);

            bool errorAlMarcarRegistroComoNoSincronizado =false;
            for (int i = 0; i < informacionSabana.Rows.Count; i++)
            {
                paramsMarcarInformacion.SetValue("Intermedia_ID", informacionSabana.Rows[i]["Intermedia_ID"]);
                paramsMarcarInformacion.SetValue("DescripcionErrorSAP", informacionSabana.Rows[i]["DescripcionErrorSAP"]);
                try
                {
                    template.ExecuteNonQuery(System.Data.CommandType.StoredProcedure, "MarcarRegistroSabanaComoNoSincronizado_SP", paramsMarcarInformacion);
                }
                catch (Exception ex)
                {
                    logger.ErrorException("MarcarInformacionComoNoSincronizada", ex);
                    errorAlMarcarRegistroComoNoSincronizado = true;
                    continue;
                }
            }
            return !errorAlMarcarRegistroComoNoSincronizado;
        }

        public bool MarcarInformacionComoSincronizada(IObjetoSincronizacionSAP objetoSincronizacion)
        {
            int codigoTransaccion =
                (int)transactionTemplate.Execute(delegate(ITransactionStatus status)
            {
                IDbParameters paramsMarcarInformacion = template.CreateDbParameters();
                paramsMarcarInformacion.Add("DocEntrySAP", System.Data.DbType.Int32);
                paramsMarcarInformacion.Add("DocNumSAP", System.Data.DbType.Int32);
                paramsMarcarInformacion.Add("Intermedia_ID", System.Data.DbType.Int32);

                System.Data.DataTable informacionSabana = objetoSincronizacion.InformacionObjeto.Tables[0];

                for (int i = 0; i < informacionSabana.Rows.Count; i++)
                {
                    paramsMarcarInformacion.SetValue("Intermedia_ID", informacionSabana.Rows[i]["Intermedia_ID"]);
                    paramsMarcarInformacion.SetValue("DocEntrySAP", informacionSabana.Rows[i]["DocEntrySAP"]);
                    paramsMarcarInformacion.SetValue("DocNumSAP", informacionSabana.Rows[i]["DocNumSAP"]);
                    try
                    {
                        template.ExecuteNonQuery(System.Data.CommandType.StoredProcedure, "MarcarRegistroSabanaComoSincronizado_SP", paramsMarcarInformacion);
                    }
                    catch (Exception ex)
                    {
                        logger.ErrorException("MarcarInformacionComoSincronizada", ex);
                        status.SetRollbackOnly();
                        return -1;
                    }
                }

                if(!objetoSincronizacion.EjecutarOperacionesPostMarcado())
                {
                    status.SetRollbackOnly();
                    return -1;
                }

                return 0;
            });
            return codigoTransaccion == 0;
        }

    }
}

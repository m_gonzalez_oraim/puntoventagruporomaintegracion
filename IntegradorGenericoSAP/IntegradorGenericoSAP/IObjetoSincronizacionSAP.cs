﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntegradorGenericoSAP
{
    public interface IObjetoSincronizacionSAP
    {
        string MensajeError { get; }
        System.Data.DataSet InformacionObjeto { set; get; }
        bool Sincronizar();
        bool EjecutarOperacionesPostMarcado();
        string TipoObjeto { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntegradorGenericoSAP
{
    public class WrapperConexionSAP
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        private SAPbobsCOM.Company conexion;
        public SAPbobsCOM.Company Conexion
        {
            get { return conexion; }
        }

        private IConfiguradorConexionSAP configuradorConexionSAP;
        public IConfiguradorConexionSAP ConfiguradorConexionSAP
        {
            get { return configuradorConexionSAP; }
            set { configuradorConexionSAP = value; }
        }

        public void DesconectarseDeSAP()
        {
            if (conexion == null)
                return;

            if (!conexion.Connected)
                return;

            conexion.Disconnect();
        }

        public bool ConectarseASAP()
        {
            conexion = new SAPbobsCOM.Company();
            configuradorConexionSAP.CargarInformacionConexionSAP(ref conexion);
            try
            {
                int codigoConexion = conexion.Connect();
                if (codigoConexion != 0)
                {
                    logger.Error("realizarConexionBaseDatosSAP:" + conexion.GetLastErrorDescription());
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                logger.ErrorException("realizarConexionBaseDatosSAP", ex);
                return false;
            }
        }

    }
}
